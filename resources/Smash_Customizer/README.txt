                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:2467523
Smash Customizer by shivinteger is licensed under the Creative Commons - Attribution - Non-Commercial license.
http://creativecommons.org/licenses/by-nc/3.0/

# Summary

**FAQ**: http://www.thingiverse.com/shivinteger/about 

[>>>] Generating Attributions log: 
 
[0] Thing Title : Bird House Customizer
[0] Thing URL : http://www.thingiverse.com/thing:106951
[0] Thing Author : TheNewHobbyist
[0] Thing Licence : Creative Commons - Attribution - Non-Commercial

[1] Thing Title : Conch (Mollusca/Gastropoda)
[1] Thing URL : http://www.thingiverse.com/thing:164182
[1] Thing Author : geofablab
[1] Thing Licence : Creative Commons - Attribution - Non-Commercial

[2] Thing Title : Alternate Labyrith Gift Box
[2] Thing URL : http://www.thingiverse.com/thing:205704
[2] Thing Author : Corona688
[2] Thing Licence : Creative Commons - Attribution - Non-Commercial

[3] Thing Title : Ike:Core - 3D printer based on CoreXY
[3] Thing URL : http://www.thingiverse.com/thing:313095
[3] Thing Author : jayftee
[3] Thing Licence : Creative Commons - Attribution - Non-Commercial

[4] Thing Title : 2011 Jetta Windshield Wiper Adapter
[4] Thing URL : http://www.thingiverse.com/thing:783450
[4] Thing Author : ES0602
[4] Thing Licence : Creative Commons - Attribution - Non-Commercial

[5] Thing Title : Hope Power Ring
[5] Thing URL : http://www.thingiverse.com/thing:940782
[5] Thing Author : svoboman
[5] Thing Licence : Creative Commons - Attribution - Non-Commercial

[6] Thing Title : Pi-Corder Case
[6] Thing URL : http://www.thingiverse.com/thing:1079419
[6] Thing Author : Raspbian
[6] Thing Licence : Creative Commons - Attribution - Non-Commercial

[7] Thing Title : Iron Works Sculpture
[7] Thing URL : http://www.thingiverse.com/thing:1100180
[7] Thing Author : onebitpixel
[7] Thing Licence : Creative Commons - Attribution - Non-Commercial

[8] Thing Title : Hermitage Castle Catan piece
[8] Thing URL : http://www.thingiverse.com/thing:1199328
[8] Thing Author : bainite
[8] Thing Licence : Creative Commons - Attribution - Non-Commercial

[9] Thing Title : cold weather
[9] Thing URL : http://www.thingiverse.com/thing:1313354
[9] Thing Author : dferraros
[9] Thing Licence : Creative Commons - Attribution - Non-Commercial

[10] Thing Title : Super Smash Bros. Trophy
[10] Thing URL : http://www.thingiverse.com/thing:1363128
[10] Thing Author : PterodactylDanceParty
[10] Thing Licence : Creative Commons - Attribution - Non-Commercial

[11] Thing Title : Hamburg Matten Corner Aquarium Filter
[11] Thing URL : http://www.thingiverse.com/thing:1512580
[11] Thing Author : ThePrintingSaffa
[11] Thing Licence : Creative Commons - Attribution - Non-Commercial

[+] Attributions logged 


[+] Getting object #0
[<] Importing : downloads/856959/uss_joseph_kennedy_jr.stl
[+] Cleaning non manifold
[!] Total removed V: -3.47%, E: -3.60%, F: -3.69%
[-] Object is malformed
[+] Deleting bad source : downloads/856959/uss_joseph_kennedy_jr.stl

[+] Getting object #0
[<] Importing : downloads/313095/Ike_Core_v1_-_Heatbed_screw_anchor_B.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : Ike Core v1 - Heatbed screw anchor B

[+] Getting object #1
[<] Importing : downloads/19594/Biped_Brackets.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : Biped Brackets
[+] Rotating object = Biped Brackets
[!] Volume difference is 110.94%
[+] Created duplicate : Ike Core v1 - Heatbed screw anchor B.001
[+] Staging objects : BOTH_LEFT
[+] Union between : Ike Core v1 - Heatbed screw anchor B.001 & Biped Brackets
[!] Number of faces #1 : 460 + number of faces #2 : 460 = 920
[!] Number of faces of union: 948
[+] Cleaning non manifold
[!] Total removed V: -40.65%, E: -38.00%, F: -35.02%
[-] Object is malformed
[-] Union did not work well. Discarding object.

[+] Getting object #2
[<] Importing : downloads/1363128/SSBTrophyBase.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : SSBTrophyBase
[+] Rotating object = SSBTrophyBase
[!] Volume difference is 1812.54%
[+] Scaled SSBTrophyBase by 82.02%
[+] Created duplicate : Ike Core v1 - Heatbed screw anchor B.001
[+] Staging objects : BOTH_TOP
[+] Union between : Ike Core v1 - Heatbed screw anchor B.001 & SSBTrophyBase
[!] Number of faces #1 : 460 + number of faces #2 : 2048 = 2508
[!] Number of faces of union: 2367
[+] Cleaning non manifold
[!] Total removed V: -0.29%, E: -0.21%, F: -0.13%

[+] Getting object #3
[<] Importing : downloads/1100180/iron_relief_left_crack_removedmirror.stl
[+] Cleaning non manifold
[!] Total removed V: -0.01%, E: -0.01%, F: -0.01%
[+] Preparing : Iron Relief Left Crack Removedmirror
[+] Reducing the number of polygons by 29.22%
[+] Rotating object = Iron Relief Left Crack Removedmirror.001
[!] Volume difference is 548.03%
[+] Created duplicate : Ike Core v1 - Heatbed screw anchor B.000
[+] Staging objects : ON_BACK
[+] Union between : Ike Core v1 - Heatbed screw anchor B.000 & Iron Relief Left Crack Removedmirror.001
[!] Number of faces #1 : 2364 + number of faces #2 : 100000 = 102364
[!] Number of faces of union: 101129
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%

[+] Getting object #4
[<] Importing : downloads/1313354/customizable_burma-shave_tnh_Extruder220160203-25899-1jnbzq9-0.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : customizable burma-shave tnh Extruder220160203-25899-1jnbzq9-0
[+] Rotating object = customizable burma-shave tnh Extruder220160203-25899-1jnbzq.060
[!] Volume difference is 0.00%
[+] Scaled customizable burma-shave tnh Extruder220160203-25899-1jnbzq.060 by 2757.53%
[+] Created duplicate : Ike Core v1 - Heatbed screw anchor B.001
[+] Staging objects : BOTH_LEFT
[+] Union between : Ike Core v1 - Heatbed screw anchor B.001 & customizable burma-shave tnh Extruder220160203-25899-1jnbzq.060
[!] Number of faces #1 : 101129 + number of faces #2 : 496 = 101625
[!] Number of faces of union: 97130
[+] Cleaning non manifold
[!] Total removed V: -0.00%, E: -0.00%, F: 0.00%

[+] Getting object #5
[<] Importing : downloads/1199328/Hermitage.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : Hermitage
[+] Rotating object = Hermitage
[!] Volume difference is 0.11%
[+] Scaled Hermitage by 447.75%
[+] Created duplicate : Ike Core v1 - Heatbed screw anchor B.000
[+] Staging objects : DEFAULT
[+] Union between : Ike Core v1 - Heatbed screw anchor B.000 & Hermitage
[!] Number of faces #1 : 97130 + number of faces #2 : 3064 = 100194
[!] Number of faces of union: 95156
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%

[+] Getting object #6
[<] Importing : downloads/940782/power_ring_v1_20150725-25888-7wwbh-0.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : Power Ring V1 20150725-25888-7Wwbh-0
[+] Rotating object = Power Ring V1 20150725-25888-7Wwbh-0.001
[!] Volume difference is 0.54%
[+] Scaled Power Ring V1 20150725-25888-7Wwbh-0.001 by 264.53%
[+] Created duplicate : Ike Core v1 - Heatbed screw anchor B.001
[+] Staging objects : ON_LEFT
[+] Union between : Ike Core v1 - Heatbed screw anchor B.001 & Power Ring V1 20150725-25888-7Wwbh-0.001
[!] Number of faces #1 : 95156 + number of faces #2 : 2108 = 97264
[!] Number of faces of union: 97141
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%

[+] Getting object #7
[<] Importing : downloads/1347772/alphabet.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : Alphabet
[+] Rotating object = Alphabet.004
[!] Volume difference is 0.03%
[+] Scaled Alphabet.004 by 720.42%
[+] Created duplicate : Ike Core v1 - Heatbed screw anchor B.000
[+] Staging objects : ON_FRONT
[+] Union between : Ike Core v1 - Heatbed screw anchor B.000 & Alphabet.004
[!] Number of faces #1 : 97141 + number of faces #2 : 144 = 97285
[!] Number of faces of union: 97439
[+] Cleaning non manifold
[!] Total removed V: -2.66%, E: -2.19%, F: -1.90%
[-] Object is malformed
[-] Union did not work well. Discarding object.

[+] Getting object #8
[<] Importing : downloads/1512580/HamburgMattenSubstrate_bottom.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : HamburgMattenSubstrate bottom
[+] Rotating object = HamburgMattenSubstrate bottom
[!] Volume difference is 9.73%
[+] Scaled HamburgMattenSubstrate bottom by 100.92%
[+] Created duplicate : Ike Core v1 - Heatbed screw anchor B.000
[+] Staging objects : BOTH_BACK
[+] Union between : Ike Core v1 - Heatbed screw anchor B.000 & HamburgMattenSubstrate bottom
[!] Number of faces #1 : 97141 + number of faces #2 : 1652 = 98793
[!] Number of faces of union: 98233
[+] Cleaning non manifold
[!] Total removed V: -0.00%, E: -0.00%, F: 0.00%

[+] Getting object #9
[<] Importing : downloads/106951/Downy_Woodpecker_TNH.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : Downy Woodpecker TNH
[+] Rotating object = Downy Woodpecker TNH.002
[!] Volume difference is 0.00%
[+] Scaled Downy Woodpecker TNH.002 by 1668.35%
[+] Created duplicate : Ike Core v1 - Heatbed screw anchor B.001
[+] Staging objects : ON_RIGHT
[+] Union between : Ike Core v1 - Heatbed screw anchor B.001 & Downy Woodpecker TNH.002
[!] Number of faces #1 : 98233 + number of faces #2 : 28 = 98261
[!] Number of faces of union: 98134
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%

[+] Getting object #10
[<] Importing : downloads/783450/JettaWiperAdapterv1_2Top.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: -0.55%, F: -0.82%
[+] Preparing : JettaWiperAdapterv1 2Top
[+] Rotating object = JettaWiperAdapterv1 2Top
[!] Volume difference is 0.11%
[+] Scaled JettaWiperAdapterv1 2Top by 456.64%
[+] Created duplicate : Ike Core v1 - Heatbed screw anchor B.000
[+] Staging objects : ON_RIGHT
[+] Union between : Ike Core v1 - Heatbed screw anchor B.000 & JettaWiperAdapterv1 2Top
[!] Number of faces #1 : 98134 + number of faces #2 : 845 = 98979
[!] Number of faces of union: 98973
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%

[+] Getting object #11
[<] Importing : downloads/164182/Conch.stl
[+] Cleaning non manifold
[!] Total removed V: -0.02%, E: -0.02%, F: -0.02%
[+] Preparing : Conch
[+] Reducing the number of polygons by 74.97%
[+] Rotating object = Conch
[!] Volume difference is 5.17%
[+] Scaled Conch by 124.63%
[+] Created duplicate : Ike Core v1 - Heatbed screw anchor B.001
[+] Staging objects : ON_BOTTOM
[+] Union between : Ike Core v1 - Heatbed screw anchor B.001 & Conch
[!] Number of faces #1 : 98973 + number of faces #2 : 100002 = 198975
[!] Number of faces of union: 197039
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%

[+] Getting object #12
[<] Importing : downloads/1079419/picorder-cover.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : Picorder-Cover
[+] Rotating object = Picorder-Cover
[!] Volume difference is 1.35%
[+] Scaled Picorder-Cover by 194.78%
[+] Created duplicate : Ike Core v1 - Heatbed screw anchor B.000
[+] Staging objects : ON_RIGHT
[+] Union between : Ike Core v1 - Heatbed screw anchor B.000 & Picorder-Cover
[!] Number of faces #1 : 197039 + number of faces #2 : 512 = 197551
[!] Number of faces of union: 197585
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%

[+] Getting object #13
[<] Importing : downloads/205704/blank.STL
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : blank
[+] Rotating object = blank
[!] Volume difference is 0.84%
[+] Scaled blank by 228.47%
[+] Created duplicate : Ike Core v1 - Heatbed screw anchor B.001
[+] Staging objects : DEFAULT
[+] Union between : Ike Core v1 - Heatbed screw anchor B.001 & blank
[!] Number of faces #1 : 197585 + number of faces #2 : 1484 = 199069
[!] Number of faces of union: 193197
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%

[>] Exporting STL to : /uploads/20170804-0554-34