shader_type canvas_item;
//render_mode blend_add;

uniform float decal_red_x;
uniform float decal_red_y;
uniform float decal_green_x;
uniform float decal_green_y;
uniform float decal_blue_x;
uniform float decal_blue_y;

uniform float y_limit;

void fragment(){
	if ( UV.y < y_limit ) {
		float r = texture( TEXTURE, UV + vec2( decal_red_x, decal_red_y ) ).r;
		float g = texture( TEXTURE, UV + vec2( decal_green_x, decal_green_y ) ).g;
		float b = texture( TEXTURE, UV + vec2( decal_blue_x, decal_blue_y ) ).b;
		COLOR = vec4( r,g,b,1 );
	} else {
		COLOR = texture( TEXTURE, UV );
	}
}