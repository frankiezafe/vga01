extends Node2D

var labels = []
var previous = 0
var lid = 0

func _ready():
	for i in range(0,3):
		labels.append( get_node( "label" + str(i) ) )
	pass

func _on_click_me_pressed():
	for l in labels:
		l.visible = !l.visible