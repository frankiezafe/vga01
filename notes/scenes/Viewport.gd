extends Spatial

var screensize

func adapt():
	screensize = OS.window_size
	get_node( "Viewport" ).size = OS.window_size
	get_node( "render" ).position = OS.window_size * 0.5

func _ready():
	adapt()

func _process(delta):
	if screensize != OS.window_size:
		adapt()
