tool
extends VisualInstance

var framecount = 0
var rotation_authorised = true
var rotation_enabled = false

export(float, 0, 20) var rotation_speed = 8

func _ready():
	#print( "tri ready" )
	pass

func _input(event):
	if rotation_authorised:
		#print( event )
		if event is InputEventMouseMotion:
			#print( event.position, event.relative )
			global_translate( Vector3( event.relative.x * 0.01, 0, event.relative.y * 0.01 ) )
		elif event is InputEventMouseButton:
			# tourne uniquement quand bouton pressé
			#if event.button_index == 1:
			#	rotation_enabled = event.pressed
			# enclenche et déclenche la rotation
			if event.button_index == 1 and event.pressed:
				if rotation_enabled:
					rotation_enabled = false
				else:
					rotation_enabled = true

func _process(delta):
#	#print( "tri process " + str( delta ) + " - " + str( framecount ) )
	framecount = framecount + 1
	if rotation_enabled:
		rotate_y( delta * rotation_speed )

func _on_click_me_mouse_entered():
	rotation_authorised = false
	pass # replace with function body

func _on_click_me_mouse_exited():
	rotation_authorised = true
	pass # replace with function body
