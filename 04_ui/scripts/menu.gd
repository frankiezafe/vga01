extends Node2D

var btn = null
var info = null
var info_pos = null
var info_pos_target = null
var sprites = []
var sa = 0
var sa_target = 0

func sprite_alpha( a ):
	if info:
		info.modulate.a = a
		var ai = ( 1 + sin( -PI*0.5 + a * PI ) ) * 0.5
		info.position = ( info_pos_target * ai ) + ( info_pos * ( 1 - ai ) )
		for s in sprites:
			s.modulate.a = ai * 0.9
		if not info.visible:
			info.visible = true

func _ready():
	btn = get_node( "info_btn" )
	info = get_node( "info_content" )
	if info:
		info_pos = Vector2( -300,0 )
		info_pos_target = Vector2( -300,0 )
	sprites.append( get_node( "info_content/shivinteger" ) )
	sprites.append( get_node( "info_content/preview" ) )
	sprite_alpha( sa )

func _process(delta):
	if info:
		if sa < sa_target:
			sa += delta * 2
			if sa > sa_target:
				sa = sa_target
		elif sa > sa_target:
			sa -= delta * 4
			if sa < sa_target:
				sa = sa_target
		sprite_alpha( sa )

func _on_info_btn_pressed():
	if info:
		if sa_target == 0:
			info_pos = Vector2( info.position.x, info.position.y )
			info_pos_target = Vector2( 0,0 )
			sa_target = 1
		else:
			info_pos = Vector2( -300,0 )
			info_pos_target = Vector2( info.position.x, info.position.y )
			sa_target = 0
