tool

extends Spatial

var y_speed = 0
var y_speed_target = 0.07
var mousepos = Vector2(0,0)

export(bool) var debug = false
export(float, -2, 2) var speed_horizontal = 1 setget sh
export(float, -2, 2) var speed_vertical = 1 setget sv

func sh(value):
	speed_horizontal = value

func sv(value):
	speed_vertical = value

func _ready():
	pass # Replace with function body.

func _input(event):
	if debug:
		print( event )
	#if (event is InputEventMouseButton and event.is_pressed() == true):
	if (event is InputEventMouseMotion ):
		if debug:
			print( event.relative / OS.window_size )
		var tmp = mousepos + (event.relative / OS.window_size)
		mousepos += (tmp - mousepos) * 0.2

func _process(delta):
#	y_speed += ( y_speed_target - y_speed ) * 0.1 * delta
	rotate_x( mousepos.y * speed_vertical )
	rotate_y( mousepos.x * speed_horizontal )
#	mousepos *= 0.95
	pass
